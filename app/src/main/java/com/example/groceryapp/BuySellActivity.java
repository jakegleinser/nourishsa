package com.example.groceryapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class BuySellActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_sell);
    }

    public void buy(View view) {
        Intent senderIntent = getIntent();
        String message = senderIntent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        if (message.equals("LOGIN"))
        {
            Intent intent = new Intent(this, BuyerLoginActivity.class);
            startActivity(intent);
        }
        else if (message.equals(("SIGN UP")))
        {
            Intent intent = new Intent(this, BuyerSignUpActivity.class);
            startActivity(intent);
        }
    }

    public void sell(View view) {
        Intent senderIntent = getIntent();
        String message = senderIntent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        if (message.equals("LOGIN"))
        {
            Intent intent = new Intent(this, SellerLoginActivity.class);
            startActivity(intent);
        }
        else if (message.equals(("SIGN UP")))
        {
            Intent intent = new Intent(this, SellerSignUpActivity.class);
            startActivity(intent);
        }
    }

}