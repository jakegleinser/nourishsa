package com.example.groceryapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class SellerLoginActivity extends AppCompatActivity {

    public static final String USERNAME = "com.example.groceryapp.USERNAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_login);
    }

    public void logInSeller(View view) {
        Intent intent = new Intent(this, ListingFormActivity.class);

        EditText editText = findViewById(R.id.username);
        String username = editText.getText().toString();
        intent.putExtra(USERNAME, username);
        //because prototype, we do not care at all what password is here, we are doing zero authentication

        startActivity(intent);
        Log.d("LoginButtonClicked", "editText");
    }
}