package com.example.groceryapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.groceryapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /** Called when the user taps the Log In button */
    public void logIn(View view) {
        Intent intent = new Intent(this, BuySellActivity.class);
        /*
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
         */
        intent.putExtra(EXTRA_MESSAGE, "LOGIN");
        startActivity(intent);


        //Log.d("ButtonClicked", message);
    }

    /** Called when the user taps the Sign Up button */
    public void signUp(View view) {
        Intent intent = new Intent(this, BuySellActivity.class);

        intent.putExtra(EXTRA_MESSAGE, "SIGN UP");
        startActivity(intent);
    }

}